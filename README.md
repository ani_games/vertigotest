# INTERACTION SYSTEM #

* This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This Repository represents the Unity project that can be considered as a result of tech assignment

### How do I get set up? ###

* git clone https://ani_games@bitbucket.org/ani_games/vertigotest.git
* Open in Unity (2020.3.4)
* Open the scene Scenes/Init
* Run the game

### Description ###
* You can interract with any dinamic objects and doors around you
* Playing basketball is pretty funny :)

### Author ###

* Anatoly Ivanov
* kelley@mail.ru