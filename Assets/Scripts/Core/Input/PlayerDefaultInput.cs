using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;
using VertigoTest.Messages;

namespace VertigoTest.GameInput
{
    public class PlayerDefaultInput : InputBase
    {
        [SerializeField] private Transform _head;
        [Range(0.01f,1f)]
        [SerializeField] private float _speed = 0.33f;
        [Range(0.01f, 1f)] 
        [SerializeField] private float _angularSpeed = 0.33f;
        
        private float _rotation = 0f;
        private float _observe = 0f;

        private void Start()
        {
            //#if UNITY_EDITOR
            //Cursor.lockState = CursorLockMode.Confined;
            //#endif
        }

        private void FixedUpdate()
        {
            if (!Input.GetKey(KeyCode.LeftControl))
            {
                Rotate(Vector3.up * (UnityEngine.Input.mousePosition.x-_rotation) * _angularSpeed);
                Observe((Input.mousePosition.y-_observe)*-_angularSpeed);
            }
            
            _rotation = Input.mousePosition.x;
            _observe = Input.mousePosition.y;

            if (Input.GetKey(KeyCode.W))
            {
                Move(Vector3.forward*_speed);
            }
            if (Input.GetKey(KeyCode.S))
            {
                Move(Vector3.back*_speed);
            }
            if (Input.GetKey(KeyCode.A))
            {
                Move(Vector3.left*_speed);
            }
            if (Input.GetKey(KeyCode.D))
            {
                Move(Vector3.right*_speed);
            }
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                MessageManager.Interracting.Interract.Publish(0);
            }
            if (Input.GetKeyDown(KeyCode.R))
            {
                MessageManager.Interracting.Throw.Publish(_head.forward);
            }

            if (Input.GetKeyDown(KeyCode.F))
            {
                MessageManager.Hands.RightHandUseStart.Publish(_head.forward);
            }

            if (Input.GetKey(KeyCode.F))
            {
                MessageManager.Hands.RightHandInLongUse.Publish(_head.forward);
            }
            
            if (Input.GetKeyDown(KeyCode.C))
            {
                MessageManager.Hands.LeftHandUseStart.Publish(_head.forward);
            }

            if (Input.GetKey(KeyCode.C))
            {
                MessageManager.Hands.LeftHandInLongUse.Publish(_head.forward);
            }
            
            if (Input.GetKey(KeyCode.Escape))
            {
                Application.Quit();
            }
        }
    }
}

