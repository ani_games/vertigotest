using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VertigoTest.GameInput
{
    public abstract class InputBase : MonoBehaviour
    {
        [SerializeField] private Mover.Movable _movable;

        protected virtual void Move(Vector3 velocity)
        {
            _movable.Move(velocity);
        }

        protected virtual void Rotate(Vector3 angularVelocity)
        {
            _movable.Rotate(angularVelocity);
        }

        protected virtual void Observe(float deltaAngle)
        {
            _movable.Observe(deltaAngle);
        }
    }
}

