using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VertigoTest.Shootable
{
    [CreateAssetMenu(fileName = "ShootableModel", menuName = "Scriptable/ShootableModel")]
    public class ShootableModel : ScriptableObject, IShootableModel
    {
        [SerializeField] private bool _isAutomatic;
        [SerializeField] private float _cooldownTime;
        [SerializeField] private float _reloadingTime;

        public bool IsAutomatic => _isAutomatic;
        public float CoolDownTime => _cooldownTime;
        public float ReloadingTIme => _reloadingTime;
    }
}


