using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VertigoTest.Shootable
{
    public class Ammo : IAmmo
    {
        public int MaxBullets { get; set; }
        public int RestBullets { get; set; }

        public Ammo(int max, int rest)
        {
            MaxBullets = max;
            RestBullets = rest;
        }
    }
}

