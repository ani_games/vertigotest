using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VertigoTest.Shootable
{
    public interface IAmmo
    {
        int MaxBullets { get; set; }
        int RestBullets { get; set; }
    }
}

