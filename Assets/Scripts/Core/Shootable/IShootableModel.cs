using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VertigoTest.Shootable
{
    public interface IShootableModel
    {
        bool IsAutomatic { get; }

        float CoolDownTime { get; }
        
        float ReloadingTIme { get; }
    }    
}


