using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VertigoTest.Shootable
{
    public class Gun : Shootable
    {
        [SerializeField] private Animator _animator;

        protected override void OnShotComplete()
       {
           base.OnShotComplete();
           _animator.Play("Fire");
       }

       protected override void StartReloading()
        {
            StartCoroutine(ReloadCoroutine());
            _animator.Play("Reload");
        }

        protected override void NotEnoughAmo()
        {
            base.NotEnoughAmo();
            _animator.Play("Empty");
        }

        private IEnumerator ReloadCoroutine()
        {
            yield return new WaitForSeconds(ReloadingTime);
            OnAmmoReloaded();
        }
    }
}

