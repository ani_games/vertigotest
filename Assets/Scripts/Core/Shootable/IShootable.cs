using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VertigoTest.Shootable
{
    public interface IShootable
    {
        void Init(IShootableModel model, List<IAmmo> ammos);
        bool IsAutomatic { get; }
        float CoolDownTime { get; }
        float ReloadingTime { get; }
        int CurrentAmmoBullets { get; }
        int TotalBullets { get; }
        void SingleShoot(Vector3 direction);
        void Autoshoot(Vector3 direction);
        void Reload();
    }
}

