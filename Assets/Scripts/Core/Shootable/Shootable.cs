using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using VertigoTest.Interacting;
using VertigoTest.Messages;

namespace VertigoTest.Shootable
{
    public class Shootable : PortableItem, IShootable
    {
        [SerializeField] private ShootableModel _model;
        private List<IAmmo> _ammos;

        private float _lastShotTime;
        private float _lastReloadTime;
        
        public bool IsAutomatic { get; private set; }
        public float CoolDownTime { get; private set; }
        public float ReloadingTime { get; private set;}
        
        public int CurrentAmmoBullets
        {
            get
            {
                if (_ammos.Count == 0)
                {
                    return 0;
                }

                return _ammos[0].RestBullets;
            }
        }

        public int TotalBullets => _ammos.Sum(t => t.RestBullets);

        private void Start()
        {
            Init(_model,new List<IAmmo>(){new Ammo(5,8), new Ammo(8,8)});
        }

        public void Init(IShootableModel model, List<IAmmo> ammos)
        {
            _ammos = ammos;
            IsAutomatic = model.IsAutomatic;
            CoolDownTime = model.CoolDownTime;
            ReloadingTime = model.ReloadingTIme;

        }

        public override void Interact(IInteractor interractor, int actionId)
        {
            base.Interact(interractor, actionId);
            MessageManager.Shooting.UpdateAmmo.Publish(this);
        }

        public void SingleShoot(Vector3 direction)
        {
            if (!IsAutomatic)
            {
                Shoot();
            }
        }

        public void Autoshoot(Vector3 direction)
        {
            if (IsAutomatic)
            {
                Shoot();
            }
        }

        public void Reload()
        {
            if (Time.time - _lastReloadTime > ReloadingTime)
            {
                _lastReloadTime = Time.time;
                StartReloading();
            }
        }

        protected virtual void StartReloading()
        {
            OnAmmoReloaded();
        }

        protected void OnAmmoReloaded()
        {
            if (_ammos.Count > 0)
            {
                _ammos.RemoveAt(0);
            }
            MessageManager.Shooting.UpdateAmmo.Publish(this);
        }
        
        private void Shoot()
        {

            if (Time.time - _lastShotTime > CoolDownTime 
                && Time.time - _lastReloadTime > ReloadingTime)
            {
                if (_ammos.Count > 0)
                {
                    if (_ammos[0].RestBullets > 0)
                    {
                        _ammos[0].RestBullets--;
                        _lastShotTime = Time.time;
                        OnShotComplete();
                        if (_ammos[0].RestBullets == 0)
                        {
                            Reload();
                        }
                    }
                    else
                    {
                        Reload();
                        NotEnoughAmo();
                        return;
                    }
                }
                else
                {
                    NotEnoughAmo();
                    return;
                }
            }
        }

        protected virtual void OnShotComplete()
        {
            MessageManager.Shooting.Shoot.Publish(this);
            MessageManager.Shooting.UpdateAmmo.Publish(this);
        }
        
        protected virtual void NotEnoughAmo()
        {
            
        }


    }
}

