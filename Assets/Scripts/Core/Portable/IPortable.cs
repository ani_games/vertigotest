using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VertigoTest.Portable
{
    public interface IPortable
    {
        IImpulse impulse { get; }
        void Attach(ISlot slot);
        void Detache(ISlot slot, Vector3 direction);
        
        bool InUse { get; set; }
    }
}
