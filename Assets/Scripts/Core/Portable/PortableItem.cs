using System.Collections;
using System.Collections.Generic;
using System.Linq;
using MyNamespace;
using UnityEngine;
using VertigoTest.Interacting;
using VertigoTest.Portable;

public class PortableItem : Interactable, IPortable
{
    [SerializeField] private Rigidbody _rigidbody;
    [SerializeField] private ImpulseData _impulseData;
    [SerializeField] private SlotType[] _slotTypes;
    
    private Transform _formerParent;
    public IImpulse impulse => _impulseData;

    public ISlot _usedSlot = null;
    
    public bool InUse 
    {
        get;
        set;
    }

    public virtual void Attach(ISlot slot)
    {
        InUse = true;
        IsInteractionEnable = false;
        _rigidbody.collisionDetectionMode = CollisionDetectionMode.ContinuousSpeculative;
        _rigidbody.isKinematic = true;
        _formerParent = transform.parent;
        Collider[] colliders = GetComponents<Collider>();
        colliders.Any(t => t.enabled = false);
        transform.SetParent(slot.Parent);
        transform.localPosition = Vector3.zero;
        transform.localRotation = Quaternion.identity;
        _usedSlot = slot;
    }

    public virtual void Detache(ISlot slot, Vector3 direction)
    {
        _rigidbody.isKinematic = false;
        transform.SetParent(_formerParent);
        Collider[] colliders = GetComponents<Collider>();
        colliders.Any(t => t.enabled = true);
        Vector3 velocity = direction * impulse.Velocity + impulse.ExtraGlobalVelocity;
        _rigidbody.collisionDetectionMode = CollisionDetectionMode.Continuous;
        _rigidbody.velocity = velocity;
        _rigidbody.angularVelocity = impulse.AngularVelocity;

        InUse = false;
        IsInteractionEnable = true;
        _usedSlot = null;
    }

    public override void Interact(IInteractor interractor, int actionId)
    {
        if (interractor is IHolder holder)
        {
            switch (actionId)
            {
                case 0:
                    ISlot slot = holder.GetSlot(_slotTypes);
                    if (slot != null)
                    {
                        holder.PutItem(slot, this);
                        Attach(slot);   
                    }
                    break;
                case 1:
                    break;
            }
        }
    }
}
