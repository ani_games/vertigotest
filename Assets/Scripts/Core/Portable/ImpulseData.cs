using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VertigoTest.Portable
{
    public interface IImpulse
    {
        float Velocity { get; }
        Vector3 AngularVelocity { get; }
        
        Vector3 ExtraGlobalVelocity { get; }
    }
    public class ImpulseData : MonoBehaviour, IImpulse
    {

        [SerializeField] private float velocity;
        [SerializeField] private Vector3 extraGlobalImpulse;
        [SerializeField] private Vector3 angularVelocity;

        public float Velocity => velocity;
        public Vector3 AngularVelocity => angularVelocity;

        public Vector3 ExtraGlobalVelocity => extraGlobalImpulse;
    }
}

