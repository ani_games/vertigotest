using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using VertigoTest.Interacting;
using VertigoTest.Messages;
using VertigoTest.Shootable;

namespace VertigoTest.Portable
{
    public class ShootableItemHolder : ItemHolder
    {
        private Dictionary<ISlot, IShootable> _currentShootable = new Dictionary<ISlot, IShootable>();

        protected override void AfterStart()
        {
            base.AfterStart();
            MessageManager.Hands.LeftHandUseStart.Subscribe(SingleShootLeft);
            MessageManager.Hands.LeftHandInLongUse.Subscribe(AutoShootLeft);
            MessageManager.Hands.RightHandUseStart.Subscribe(SingleShootRight);
            MessageManager.Hands.RightHandInLongUse.Subscribe(AutoShootRight);
        }

        protected override void AfterOnDestroy()
        {
            base.AfterOnDestroy();
            MessageManager.Hands.LeftHandUseStart.Unsubscribe(SingleShootLeft);
            MessageManager.Hands.LeftHandInLongUse.Unsubscribe(AutoShootLeft);
            MessageManager.Hands.RightHandUseStart.Unsubscribe(SingleShootRight);
            MessageManager.Hands.RightHandInLongUse.Unsubscribe(AutoShootRight);
        }

        public override void PutItem(ISlot slot, IPortable portableItem)
        {
            base.PutItem(slot, portableItem);
            if (portableItem is IShootable shootable)
            {
                _currentShootable.Add(slot,shootable);
            }
            
        }

        public override void RemoveItem(IPortable portableItem, ISlot slot, Vector3 direction)
        {
            base.RemoveItem(portableItem, slot, direction);
            if (portableItem is IShootable shootable)
            {
                if (_currentShootable.ContainsValue(shootable))
                {
                    var s = _currentShootable.First(t => t.Value.Equals(shootable)).Key;
                    _currentShootable.Remove(s);
                }
            }
        }

        private void SingleShootLeft(Vector3 direction)
        {
            foreach (var shootable in _currentShootable)
            {
                if (shootable.Key.SlotType == SlotType.LeftArm)
                {
                    shootable.Value.SingleShoot(direction);
                }   
            }
        }
        
        private void AutoShootLeft(Vector3 direction)
        {
            foreach (var shootable in _currentShootable)
            {
                if (shootable.Key.SlotType == SlotType.LeftArm)
                {
                    shootable.Value.Autoshoot(direction);
                }   
            }
        }
        
        private void SingleShootRight(Vector3 direction)
        {
            foreach (var shootable in _currentShootable)
            {
                if (shootable.Key.SlotType == SlotType.RightArm)
                {
                    shootable.Value.SingleShoot(direction);
                }   
            }
        }
        
        private void AutoShootRight(Vector3 direction)
        {
            foreach (var shootable in _currentShootable)
            {
                if (shootable.Key.SlotType == SlotType.RightArm)
                {
                    shootable.Value.Autoshoot(direction);
                }   
            }
        }
    }

}

