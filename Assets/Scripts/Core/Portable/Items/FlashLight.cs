using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VertigoTest.Portable;

namespace VertigoTest.Portable
{
    public class FlashLight : Switchable
    {
        [SerializeField] private Light _light;
        
        protected override void OnSwitch()
        {
            _light.enabled = IsOn;
        }
    }
}

