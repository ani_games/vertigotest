using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VertigoTest.Portable
{
    public interface IHolder
    {
        ISlot GetSlot(SlotType[] slotTypes);
        void PutItem(ISlot slot, IPortable portableItem);
        void RemoveItem(IPortable portableItem, ISlot slot, Vector3 direction);
    }   
}
