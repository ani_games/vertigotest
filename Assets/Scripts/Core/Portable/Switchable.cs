using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VertigoTest.Messages;

namespace VertigoTest.Portable
{
    public abstract class Switchable : PortableItem
    {

        protected bool IsOn;

        public override void Attach(ISlot slot)
        {
            base.Attach(slot);
            if (slot.SlotType == SlotType.LeftArm)
            {
                MessageManager.Hands.LeftHandUseStart.Subscribe(Activate);
            }

            if (slot.SlotType == SlotType.RightArm)
            {
                MessageManager.Hands.RightHandUseStart.Subscribe(Activate);
            }
        }

        public override void Detache(ISlot slot, Vector3 direction)
        {
            base.Detache(slot, direction);
            if (slot.SlotType == SlotType.LeftArm)
            {
                MessageManager.Hands.LeftHandUseStart.Unsubscribe(Activate);
            }

            if (slot.SlotType == SlotType.RightArm)
            {
                MessageManager.Hands.RightHandUseStart.Unsubscribe(Activate);
            }
           
            
        }

        private void Activate(Vector3 direction)
        {
            IsOn = !IsOn;
            OnSwitch();
        }

        protected abstract void OnSwitch();
    }
}

