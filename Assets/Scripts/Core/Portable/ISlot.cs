using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VertigoTest.Portable
{
    public interface ISlot
    {
        Transform Parent { get; }

        SlotType SlotType { get; }
    }
}

