using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using VertigoTest.Interacting;
using VertigoTest.Messages;

namespace VertigoTest.Portable
{
    public class ItemHolder : ItemInteractor, IHolder
    {
        [SerializeField] private PlayerSlot[] _slots;

        private Dictionary<ISlot, IPortable> _items = new Dictionary<ISlot, IPortable>();

        protected override void AfterStart()
        {
            base.AfterStart();
            MessageManager.Interracting.Throw.Subscribe(RemoveFirsgtItem);
        }

        protected override void AfterOnDestroy()
        {
            base.AfterOnDestroy();
            MessageManager.Interracting.Throw.Unsubscribe(RemoveFirsgtItem);
        }

        public ISlot GetSlot(SlotType[] slotTypes)
        {
            foreach (PlayerSlot slot in _slots)
            {
                if (slotTypes.Contains(slot.SlotType) && //Holder has slot of this type and
                    !_items.Select(t=>t.Key.SlotType).Contains(slot.SlotType))//it is not already in use
                {
                    return slot;
                }
            }
            return null;
        }

        public virtual void PutItem(ISlot slot, IPortable portableItem)
        {
            if (!_items.ContainsKey(slot))
            {
                _items.Add(slot,portableItem);
            }
        }

        public virtual void RemoveItem(IPortable portableItem, ISlot slot, Vector3 direction)
        {
            if (_items.ContainsKey(slot))
            {
                _items[slot].Detache(slot, direction);
                _items.Remove(slot);
            }
        }

        private void RemoveFirsgtItem(Vector3 direction)
        {
            if (_items.Any())
            {
                var slot = _items.First(t => true).Key;
                RemoveItem(_items[slot],slot, direction);
            }
        }
    }
}

