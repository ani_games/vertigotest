using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VertigoTest.Portable;

public class PlayerSlot : MonoBehaviour, ISlot
{
    [SerializeField] private SlotType _slotType;

    public SlotType SlotType => _slotType;
    public Transform Parent => transform;
}
