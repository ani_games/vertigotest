using System.Collections;
using System.Collections.Generic;
using MyNamespace;
using UnityEngine;
using VertigoTest.Interacting;

namespace VertigoTest.Usable
{
    public class UsableObject : Interactable, IUsable
    {
        public override void Interact(IInteractor interractor, int actionId)
        {
            Use();
        }

        public virtual void Use()
        {
            
        }
    }
}

