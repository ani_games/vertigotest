using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VertigoTest.Usable;

namespace VertigoTest.Usable
{
    public class Door : UsableObject
    {
        [SerializeField] private Animator _animator;

        private bool isOpen = false;
        public override void Use()
        {
            base.Use();
            if (isOpen)
            {
                _animator.Play("DoorClose");
            }
            else
            {
                _animator.Play("DoorOpen");
            }

            isOpen = !isOpen;
        }
    }
}

