using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VertigoTest.Usable
{
    public interface IUsable
    {
        void Use();
    }   
}
