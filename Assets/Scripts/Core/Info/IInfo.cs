using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VertigoTest
{
    public interface IInfo
    {
        string GetName();
        string GetDiscription();
        Sprite GetIcon();
    }
}

