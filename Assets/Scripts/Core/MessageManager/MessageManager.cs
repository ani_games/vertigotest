using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VertigoTest.Interacting;
using VertigoTest.Shootable;

namespace VertigoTest.Messages
{
    public static class MessageManager
    {
        public static class Global
        {
            public static Message<LevelController> LevelControllerLoaded = new Message<LevelController>();
        }

        public static class Interracting
        {
            public static Message<IInteractable> InteractableDetected = new Message<IInteractable>();
            public static Message InteractableForgot = new Message();
            public static Message<int> Interract = new Message<int>();
            public static Message<Vector3> Throw = new Message<Vector3>();
        }

        public static class Shooting
        {
            public static Message<IShootable> UpdateAmmo = new Message<IShootable>();
            public static Message<IShootable> Shoot = new Message<IShootable>();
        }
        
        public static class Hands
        {
            public static Message<Vector3> RightHandUseStart = new Message<Vector3>();
            public static Message<Vector3> RightHandInLongUse = new Message<Vector3>();
            public static Message<Vector3> LeftHandUseStart = new Message<Vector3>();
            public static Message<Vector3> LeftHandInLongUse = new Message<Vector3>();
        }
    }
}

