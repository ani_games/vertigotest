using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VertigoTest.Messages
{
    public class Message
    {
        private List<System.Action> _actions = new List<Action>();

        public void Subscribe(Action action)
        {
            if (!_actions.Contains(action))
            {
                _actions.Add(action);   
            }
        }

        public void Unsubscribe(Action action)
        {
            if (_actions.Contains(action))
            {
                _actions.Remove(action);
            }
        }

        public void UnsubscribeAll()
        {
            _actions.Clear();
        }

        public void Publish()
        {
            foreach (var action in _actions)
            {
                action?.Invoke();
            }
        }
    }

    public class Message<T>
    {
        private List<System.Action<T>> _actions = new List<Action<T>>();
        
        public void Subscribe(Action<T> action)
        {
            if (!_actions.Contains(action))
            {
                _actions.Add(action);   
            }
        }

        public void Unsubscribe(Action<T> action)
        {
            if (_actions.Contains(action))
            {
                _actions.Remove(action);
            }
        }

        public void UnsubscribeAll()
        {
            _actions.Clear();
        }

        public void Publish(T data)
        {
            foreach (var action in _actions)
            {
                action?.Invoke(data);
            }
        }
    }
}


