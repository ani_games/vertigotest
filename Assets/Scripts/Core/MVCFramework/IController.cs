namespace VertigoTest.MVC
{
    public interface IController
    {
        void Init(IModel model);
    }
}