namespace VertigoTest.MVC
{
    public interface IView
    {
        void Init(IModel model);
    }
}