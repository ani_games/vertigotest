using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VertigoTest.MVC
{
    public class Controller : MonoBehaviour, IController
    {

        protected IView _view;
    
        public virtual void Init(IModel model)
        {
            _view = GetComponent<IView>();
            if (_view == null)
            {
                return;
            }

            _view.Init(model);
        }
    }   
}