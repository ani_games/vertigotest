using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VertigoTest.Messages;

namespace VertigoTest.Interacting
{
    public class ItemInteractor : Interactor
    {
        private void Start()
        {
            MessageManager.Interracting.Interract.Subscribe(OnOnterract);
            AfterStart();
        }

        private void OnDestroy()
        {
            MessageManager.Interracting.Interract.Unsubscribe(OnOnterract);
            AfterOnDestroy();
        }

        protected virtual void AfterStart()
        {
            
        }

        protected virtual void AfterOnDestroy()
        {
            
        }

        protected virtual void OnOnterract(int actionId)
        {
            Interact(actionId);
        }
        
        private void OnTriggerEnter(Collider other)
        {
            IInteractable interactable = other.GetComponent<IInteractable>();
            if (interactable != null)
            {
                if (interactable.IsInteractionEnable)
                {
                    Detect(interactable);
                }
            }
        }

        private void OnTriggerExit(Collider other)
        {
            IInteractable interactable = other.GetComponent<IInteractable>();
            if (interactable != null)
            {
                if (interactable.Equals(_currentInteractable))
                {
                    Forget();
                }
            }
        }
    }
   
}