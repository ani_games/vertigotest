using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VertigoTest.Interacting
{
    public interface IInteractable
    {
        bool IsInteractionEnable { get; }
        void Interact(IInteractor interractor, int actionId);
    }

}

