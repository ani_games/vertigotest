using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VertigoTest.Messages;

namespace VertigoTest.Interacting
{
    public abstract class Interactor : MonoBehaviour, IInteractor
    {
        protected IInteractable _currentInteractable;

        public virtual void Detect(IInteractable interactable)
        {
            _currentInteractable = interactable;
            MessageManager.Interracting.InteractableDetected.Publish(interactable);  
        }

        public virtual void Forget()
        {
            _currentInteractable = null;
            MessageManager.Interracting.InteractableForgot.Publish();
        }

        public void Interact(int actionId)
        {
            if (_currentInteractable != null)
            {
                _currentInteractable?.Interact(this,actionId);
                Forget();
            }
        }
    }

}
