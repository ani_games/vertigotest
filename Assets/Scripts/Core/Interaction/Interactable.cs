using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VertigoTest;
using VertigoTest.Interacting;

namespace MyNamespace
{
    public abstract class Interactable : MonoBehaviour, IInteractable, IInfo
    {
        [SerializeField] private string _name;
        [SerializeField] private string _description;
        [SerializeField] private Sprite _icon;

        public string GetName()
        {
            return _name;
        }

        public string GetDiscription()
        {
            return _description;
        }

        public Sprite GetIcon()
        {
            return _icon;
        }

        public bool IsInteractionEnable { get; protected set; } = true;
        public abstract void Interact(IInteractor interractor, int actionId);
    }
}


