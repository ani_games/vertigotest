using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VertigoTest.Interacting
{
    public interface IInteractor
    {
        void Detect(IInteractable interactable);

        void Forget();
        void Interact(int actionId);
    }

}
