using UnityEngine;
using VertigoTest.Messages;
using VertigoTest.MVC;

namespace VertigoTest
{
    public class LevelController : Controller
    {
        [SerializeField] private Transform _playerSpot;
        
        private ILevelData _levelData;

        public Vector3 PlayerPosition => _playerSpot.position;
        public Quaternion PlayerEulers => _playerSpot.rotation;

        private void Start()
        {
            MessageManager.Global.LevelControllerLoaded.Publish(this);
        }

        public override void Init(IModel model)
        {

            _levelData = (ILevelData) model;
            if (_levelData == null)
            {
                throw new System.Exception("Level data type mismatches");
            }

            base.Init(_levelData);
        }
    }

}