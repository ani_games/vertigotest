using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using VertigoTest.Data;
using VertigoTest.Messages;
using VertigoTest.MVC;

namespace VertigoTest
{
    public class GameController : Controller
    {
        [SerializeField] private LevelData _testLevel;
        [SerializeField] private ScriptableGameConfig _scriptableGameConfig;

        private IGameConfig _gameConfig;
        
        private ILevelData _currentLevel;
        
        //TODO: Assync loading
        private void Start()
        {
            //TODO: Add backend
            if (false /*Load config from backend*/)
            {
                //_gameConfig = Backend.GetConfig();
            }
            else
            {
                _gameConfig = _scriptableGameConfig;
            }
            
            MessageManager.Global.LevelControllerLoaded.Subscribe(InitLevel);
            LoadUI();
            LoadPlayer();
            LoadAudio();
            
            //TODO: Add level manager
            LoadLevel(_testLevel);//load test level for now
        }
        private void OnDestroy()
        {
            MessageManager.Global.LevelControllerLoaded.Unsubscribe(InitLevel);
        }

        private void LoadUI()
        {
            SceneManager.LoadScene(_gameConfig.UiScene, LoadSceneMode.Additive);
        }

        private void LoadAudio()
        {
            SceneManager.LoadScene(_gameConfig.AudioScene, LoadSceneMode.Additive);
        }

        private void LoadPlayer()
        {
            SceneManager.LoadScene(_gameConfig.PlayerScene, LoadSceneMode.Additive);
        }

        private void LoadLevel(ILevelData levelData)
        {
            _currentLevel = levelData;
            SceneManager.LoadScene("TestLevel", LoadSceneMode.Additive);
        }

        private void InitLevel(IController levelController)
        {
            levelController.Init(_currentLevel);
        }
    }
}
