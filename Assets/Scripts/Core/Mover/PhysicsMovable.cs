using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VertigoTest.Messages;

namespace VertigoTest.Mover
{
    [RequireComponent(typeof(Rigidbody))]
    public class PhysicsMovable : Movable
    {

        [SerializeField] private Rigidbody _rigidbody;
        [SerializeField] private Transform _head;

        private Vector3 _velocity;
        private Vector3 _headEulers;
        private Vector3 _bodyEulers;

        private bool _isEnable;

        private void Start()
        {
            _isEnable = false;
            MessageManager.Global.LevelControllerLoaded.Subscribe(OnLevelLoaded);
        }

        private void OnLevelLoaded(LevelController levelController)
        {
            transform.position = levelController.PlayerPosition;
            transform.rotation = levelController.PlayerEulers;
            _isEnable = true;
        }
        

        public override void Move(Vector3 velocity)
        {
            if (!_isEnable)
            {
                return;
            }
            _velocity = _rigidbody.transform.right * velocity.x + _rigidbody.transform.up * velocity.y +
                        _rigidbody.transform.forward * velocity.z;
            _rigidbody.MovePosition(_rigidbody.position+_velocity);
        }

        public override void Rotate(Vector3 angularVelocity)
        {
            if (!_isEnable)
            {
                return;
            }
            _bodyEulers += angularVelocity;
            _rigidbody.transform.localRotation = Quaternion.Euler(_bodyEulers);
        }

        public override void Observe(float deltaAngle)
        {
            if (!_isEnable)
            {
                return;
            }
            
            _headEulers += Vector3.right * deltaAngle;
            
            GetClampedAngle(ref _headEulers.x);
            _headEulers.x = Mathf.Clamp(_headEulers.x, -80, 80);

            _head.localRotation  = Quaternion.Euler(_headEulers);
        }
        
        //Create an Extension
        private void GetClampedAngle(ref float angle)
        {
            if (angle > 180)
            {
                angle -= 360;
                GetClampedAngle(ref angle);
            }

            if (angle < -180)
            {
                angle += 360;
                GetClampedAngle(ref angle);
            }
        }
    }
}


