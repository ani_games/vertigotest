using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VertigoTest.Mover
{
    public abstract class Movable : MonoBehaviour, IMovable
    {
        
        public abstract void Move(Vector3 velocity);

        public abstract void Rotate(Vector3 angularVelocity);
        
        public abstract void Observe(float deltaAngle);
    }   
}
