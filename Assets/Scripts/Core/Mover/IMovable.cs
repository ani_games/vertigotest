using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VertigoTest.Mover
{
    public interface IMovable
    {
        void Move(Vector3 velocity);
        void Rotate(Vector3 angularVelocity);

        void Observe(float deltaAngle);
    }   
}
