using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using VertigoTest.Interacting;
using VertigoTest.Messages;
using Random = UnityEngine.Random;

namespace VertigoTest.UI
{
    public class UIInteraction : MonoBehaviour
    {
        [SerializeField] private TMP_Text _interactableItem;
        void Start()
        {
            MessageManager.Interracting.InteractableDetected.Subscribe(DetectItem);
            MessageManager.Interracting.InteractableForgot.Subscribe(ForgetItem);
        }

        private void OnDestroy()
        {
            MessageManager.Interracting.InteractableDetected.Unsubscribe(DetectItem);
            MessageManager.Interracting.InteractableForgot.Unsubscribe(ForgetItem);
        }

        private void DetectItem(IInteractable item)
        {
            StopAllCoroutines();
            _interactableItem.text = item.ToString();
            if (item is IInfo info)
            {
                StartCoroutine(FillText(_interactableItem,
                    $"<size=36><b>{info.GetName()}</b></size>\n<size=24>{info.GetDiscription()}</size>"));
            }
        }

        private void ForgetItem()
        {
            StopAllCoroutines();
            _interactableItem.text = String.Empty;
        }

        private IEnumerator FillText(TMP_Text tmpText, string text)
        {
            tmpText.text = string.Empty;
            for (int i = 0; i < text.Length; i++)
            {
                tmpText.text += text[i];
                yield return new WaitForSeconds(Random.Range(0f, 0.01f));
            }
        }
    }
}


