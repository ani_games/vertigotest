using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VertigoTest
{
    public interface IGameConfig
    {
        string UiScene { get; }
        string AudioScene { get; }
        string PlayerScene { get; }
    }   
}
