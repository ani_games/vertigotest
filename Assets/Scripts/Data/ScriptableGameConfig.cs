using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

namespace VertigoTest
{
    [CreateAssetMenu(fileName = "GameConfig", menuName = "Scriptable/GameConfig")]
    public class ScriptableGameConfig : ScriptableObject, IGameConfig
    {
        [SerializeField] private string _uiScene;
        [SerializeField] private string _audioScene;
        [SerializeField] private string _playerScene;

        public string UiScene => _uiScene;
        public string AudioScene => _audioScene;
        public string PlayerScene => _playerScene;
    }
}


