using UnityEditor;
using UnityEngine.SceneManagement;
using VertigoTest.MVC;

public interface ILevelData : IModel
{
    string Id { get; }
    string SceneName { get; } 
}
