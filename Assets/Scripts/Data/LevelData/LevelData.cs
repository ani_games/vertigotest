using JetBrains.Annotations;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;
using VertigoTest.MVC;

namespace VertigoTest.Data
{
    [CreateAssetMenu(fileName = "LevelData", menuName = "Scriptable/LevelData")]
    public class LevelData : ScriptableObject, ILevelData
    {
        [SerializeField] private string _id;
        [SerializeField] private Object _scene;

        public string Id => _id;
        public string SceneName => _scene.name;
    }
}


